<?php

namespace Holmby\auth;

use \Firebase\JWT\ExpiredException;
use \Firebase\JWT\JWT;
use \Slim\Exception\HttpUnauthorizedException;
use \Slim\Exception\HttpForbiddenException;
use \Slim\Exception\HttpBadRequestException;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \stdClass;

class Authenticate {
  const HEADER_NAME = 'Auth';
  const PREFIX = 'Bearer';

  protected $publicKey;
  protected $privateKey;
  protected $lifetime;

  public function __construct($publicKey, $privateKey, $lifetime) {
    $this->publicKey = $publicKey;
    $this->privateKey = $privateKey;
    $this->lifetime = $lifetime;
  }

  /**
   * Ensure that the user is logged in.
   * @throws Exception if the jwt is missformed (pass trough from \Firebase\JWT)
   * @throws HttpUnauthorizedException if the user is not logged in or the jwt has expired
   * @throws HttpBadRequestException if the http Auth header is missformed
   * @return stdClass - the JWT's payload as a PHP object
   */
  public function authenticateUser(Request $request): stdClass {
    $payload = $this->getJwt($request);
    if ($payload == NULL) {
      throw new HttpUnauthorizedException($request, 'Du är inte inloggad.');
    }
    return $payload;
  }

  /**
   * Returns the decoded JWT from the http request header if it is present and valid
   * @throws Exception if the jwt is missformed (pass trough from \Firebase\JWT)
   * @throws HttpUnauthorizedException if the jwt has expired
   * @throws HttpBadRequestException if the http Auth header is missing or missformed
   * @return stdClass the JWT's payload as a PHP object, or NULL if the user is not logged in
   */
  public function getJwt(Request $request): ?stdClass {
    $payload = NULL;
    try {
      date_default_timezone_set('Europe/Stockholm');
      $header = $request->getHeader(static::HEADER_NAME)[0];
      if ($header) {
        $pieces = explode(" ", $header); // "Bearer JWT"
        $prefix = $pieces[0];
        if (strcmp($prefix, static::PREFIX) !== 0) {
          throw new HttpBadRequestException($request, 'illegal value for Authorization http header, expected "Bearer ...", found: ' . $prefix . ' ...');
        }
        $payload = JWT::decode($pieces[1], $this->publicKey, array('RS256'));
      }
    } catch (ExpiredException $e) {
      throw new HttpUnauthorizedException($request, 'Du har blivit automatiskt utloggad på grund av inaktivitet.');
    } catch (\Exception $e) {
      throw new HttpBadRequestException($request, 'Error decoding JWT: ' . $e->getMessage());
    }
    return $payload;
  }

  /**
   * Check if $privilege is in array "privileges" array in the JWT in the $request headers
   * To validate the jwt without cheking privileges, pass NULL to $privilege.
   * @return stdClass the JWT's payload as a PHP object.
   */
  public function authorize($privilege, $request, $publicKey): stdClass {
    $payload = Authenticate::getJwt($request);
    if (!$payload) {
      throw new HttpUnauthorizedException($request, 'Du är inte inloggad.');
    }
    if (($privilege != NULL) && !in_array($privilege, $payload->privileges)) {
      throw new HttpForbiddenException($request, 'du saknar behörighet för operationen');
    }
    return $payload;
  }

  /**
   * Create a JWT
   * @param string $user
   * @param string $privateKey - the private key used for encryption of the JWT
   * @param integer $lifetime - lifetime of the generated JWT
   * @param array $token_attr - additional attributes, associative array
   * @return string JWT as a string
   */
  public function createJwt(array $content): string {
//    assert($this->privateKey, new \Exception('Uninitialized Authenticate::privateKey, value: ' . $this->privateKey) );
    date_default_timezone_set('Europe/Stockholm');
    $issuedAt   = time();
    $expire     = $issuedAt + $this->lifetime;
    $content['exp'] = $expire;            // Expiration Time
    $payload = JWT::encode($content, $this->privateKey, 'RS256');
    return print_r($payload, true);
  }
}
