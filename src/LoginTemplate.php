<?php

/**
 * This file contains a base class for handling backend part for the Holmby auth project, https://gitlab.com/holmby/auth
 * 
 * Manage user credentials. No loggin session data is stored on the server.
 * Autentication is done by sending a jwt token in the 'Auth'
 * http header. The value for the header must be a string starting with
 * 'Bearer ', followed by the jwt token.
 */

/**
 * login()
 *
 * Do a http POST request to generate a jwt token when a user logs in.
 * Store it in the browser using Javascript, and/or localstore.
 *
 * parameters (json object in the http body):
 *   {user: username, password: password}
 * The reply body is a JSON object:
 *   { user: string,
 *     name: string,
 *     email: string,
 *     jwt: string}
 *  The jwt_token have the fields:
 *    "exp" => Expiration Time
 *    "sub" => user id
 *    "privileges" = [strings]
 */

/**
 * Change password
 *
 * Use http PATCH to change password.
 * parameters (json object in the http body):
 *   { new_password: password, token: jwt_authentication_token }
 * return: a json object:
 *   {}
 *
 */

/**
 * renewJWT()
 * The previous JWT is send in the http header, Auth
 * The response is a new JWT with an updated expiration timestamp.
 * The response is plain text.
 */

/**
 * Error response.
 * The backend uses the http status code of the reply to indicate succes/failure.
 * The response body contains more infomrmation in a JSON object:
 * {
 *    "message": "Du har blivit automatiskt utloggad.",
 *    "exception": [
 *        {
 *            "type": "Slim\\Exception\\HttpUnauthorizedException",
 *            "code": 401,
 *            "message": "Du har blivit automatiskt utloggad.",
 *            "file": "/Users/cs-pan/workspaces/shf/www/rest/holmby/Authenticate.php",
 *            "line": 36
 *        }
 *    ]
 * }
 */

namespace Holmby\Auth;
use \Slim\Exception\HttpForbiddenException;

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

abstract class LoginTemplate {
  protected $auth;

  public function __construct(Authenticate $auth) {
    $this->auth = $auth;
  }

  /**
   * Generate a new JWT, with updated exp time stamp.
   */
  public function renewJWT(Request $request, Response $response): Response {
    $oldJWT = (array) $this->auth->getJwt($request);
    if(!$oldJWT) {
      throw new HttpForbiddenException($request, 'You need to be logged in to renew session. JWT missing in request.');
    }
    $newJWT = $this->auth->createJwt($oldJWT);
    $response->getBody()->write($newJWT);
    $response = $response->withHeader('Content-type', 'text/plain');
    return $response;
  }

  /**
   * login()
   */
  public function login(Request $request, Response $response) {
    $parsedBody = $request->getParsedBody();
    $user = $parsedBody['user'];
    $password = $parsedBody['password'];
    $userArray = $this->validateUserPassword($user, $password, $request);  // throws exception if missmatch
    $jwtArray = $this->generateJwtContent($userArray);
    $authArray = array();
    $authArray['name'] = $userArray['givenName'] . ' ' . $userArray['familyName'];
    $authArray['email'] = $userArray['email'];
    $authArray['jwt'] = $this->auth->createJwt($jwtArray);
    $payload = json_encode($authArray) . "\n";
    $response->getBody()->write($payload);
    $response = $response->withHeader('Content-type', 'application/json');
    return $response;
  }

  /**
   * Return an associative aray with all values to be added to the jwt
   */
  protected function generateJwtContent($user_array) {
    $jwt_array = array();
    $jwt_array['sub'] = $user_array['id'];
    $jwt_array['privileges'] = $this->getPrivileges($user_array);
    return $jwt_array;
  }

  /**
   * Change password
   */
  /*
  public function update() {
    $new_password = $this->getParameter ( 'new_password' );
    $token = $this->getParameterOrNull ( 'token' );
    if(token != null) {
      // password reset process, authenticated by jwt in body
      try {
        $data = JWT::decode($token, HolmbyConfig::PUBLIC_KEY, array('RS256'));
        $user = $data->sub;
      } catch (ExpiredException $e) {
        throw new RESTfulException("Det har gått för lång tid från att återställning av lösenordet har begärts.",
          "jwt token has expired.");
      }

    } else {
      // logged in user
      Authenticate::authorize();
      throw new RESTfulException("Ej implementerat, byt lösenord genom att använd rutinen för att återställa lösenord",
        "Ej implementerat, byt lösenord genom att använd rutinen för att återställa lösenord",
        RESTfulException::HTTP_STATUS_SERVICE_UNAVAILABLE);
      $user = '';  // TODO, get user from auth token
      $old_password = $this->getParameter ( 'old_password' );
      $this->validateUserPassword($user, $old_password);  // throws exception if missmatch
    }

    $hash = password_hash($new_password, PASSWORD_DEFAULT);
    $query = 'UPDATE user set password=? WHERE user=?';
    $stmt = $this->mysqli->prepare ( $query );
    $stmt->bind_param ( "ss", $hash, $user );
    $stmt->execute ();
    $row_cnt = $stmt->affected_rows;

    if($row_cnt != 1){
      throw new RESTfulException("failed to update password, database error",
        "failed to update password, database error");
    }
  }
    */

  /**
   * Verify a (user, password) pair.
   * Throws an exception if the credentials are bad.
   * returns an array with the user info to be included in the readline_redisplay
   * the JWT sub field will be set to return['user'].
   */
  abstract protected function validateUserPassword($user, $password, $request);

  /**
   *  Fetch the user privileges.
   *  Returns an array which will be embedded in the JWT
   */
  abstract protected function getPrivileges($auth_array);
}
